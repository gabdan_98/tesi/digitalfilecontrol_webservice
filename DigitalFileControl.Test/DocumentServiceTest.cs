using DigitalFileControl.Api.Helpers;
using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Services.Document;
using DigitalFileControl.Model;
using DigitalFileControl.Model.Enums;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace DigitalFileControl.Test
{
    public class DocumentServiceTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task TestAddDocument()
        {
            try
            {
                DigitalFileControlDB database = new DigitalFileControlDB();
                IDocumentService documentService = new DocumentServiceSQL(database);
                LoggedUser loggedUser = new LoggedUser
                {
                    Id = Guid.Parse("992BE209-A765-47BD-1058-08DA257A523C")
                };
                var documentCount = (await documentService.GetMySharedDocuments(loggedUser)).Count;
                AddDocumentRequest request = new AddDocumentRequest
                {
                    Name = "Test document",
                    Description = "Document description",
                    DocumentExpire = false,
                    DocumentTypeId = (int)DocumentTypes.PDF,
                    Content = ""
                };
                var response = await documentService.AddDocument(request, loggedUser);
                var newDocumentCount = (await documentService.GetMySharedDocuments(loggedUser)).Count;
                Assert.AreEqual(documentCount + 1, newDocumentCount);
                await documentService.DeleteDocument(response.DocumentId, loggedUser);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Assert.Fail();
            }
        }
    }
}