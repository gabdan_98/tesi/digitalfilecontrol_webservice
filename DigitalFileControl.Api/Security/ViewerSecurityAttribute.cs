﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DigitalFileControl.Api.Security
{
    #nullable disable
    public class ViewerSecurityAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var configuration = context.HttpContext.RequestServices.GetService<IConfiguration>();

            var header = context.HttpContext.Request.Headers[configuration.GetSection("ViewerSecurity:HeaderName").Value].ToString();
            if(string.IsNullOrEmpty(header))
            {
                context.Result = new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }

            if (header != configuration.GetSection("ViewerSecurity:Key").Value)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status401Unauthorized);
            }
        }
    }
}