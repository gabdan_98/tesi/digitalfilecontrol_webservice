﻿using DigitalFileControl.Api.Models;

namespace DigitalFileControl.Api.Security
{
    public interface IJwtHelper
    {
        Tokens Authenticate(LoggedUser user);
        string GenerateRefreshToken();
    }
}