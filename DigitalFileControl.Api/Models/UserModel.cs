﻿using DigitalFileControl.Model.Models;

namespace DigitalFileControl.Api.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public List<UserDocument> UserDocuments { get; set; }
        public List<Document> OwnedDocuments { get; set; }
    }
}