﻿namespace DigitalFileControl.Api.Models.Dtos.Responses
{
    public class AddDocumentResponse
    {
        public int Id { get; set; }

        public AddDocumentResponse(int id)
        {
            Id = id;
        }
    }
}
