﻿using DigitalFileControl.Api.Models.Dtos.Responses;

namespace DigitalFileControl.Api.Models.Dtos.Response
{
    public class GetSharedWithMeDocumentsResponse : ApiResponse
    {
        public List<SharedWithMeDocument> Documents { get; set; }

        public GetSharedWithMeDocumentsResponse(List<SharedWithMeDocument> documents)
        {
            this.Documents = documents;
        }
    }
}