﻿namespace DigitalFileControl.Api.Models.Dtos.Responses
{
    public class AddUserToDocumentResponse : ApiResponse
    {
        public int Id { get; set; }

        public AddUserToDocumentResponse(int id)
        {
            Id = id;
        }
    }
}