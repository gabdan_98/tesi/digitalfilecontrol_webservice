﻿using DigitalFileControl.Api.Models.Dtos.Responses;

namespace DigitalFileControl.Api.Models.Dtos.Response
{
    public class GetMySharedDocumentsResponse : ApiResponse
    {
        public List<MySharedDocument> Documents { get; set; }

        public GetMySharedDocumentsResponse(List<MySharedDocument> documents)
        {
            this.Documents = documents;
        }
    }
}