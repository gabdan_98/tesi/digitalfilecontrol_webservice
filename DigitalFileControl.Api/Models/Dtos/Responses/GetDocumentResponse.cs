﻿using DigitalFileControl.Api.Models.Dtos.Responses;

namespace DigitalFileControl.Api.Models.Dtos.Response
{
    public class GetDocumentResponse : ApiResponse
    {
        public DocumentModel Document { get; set; }

        public GetDocumentResponse(DocumentModel document)
        {
            this.Document = document;
        }
    }
}