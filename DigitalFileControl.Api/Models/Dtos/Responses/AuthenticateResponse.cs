﻿namespace DigitalFileControl.Api.Models.Dtos.Responses
{
    public class AuthenticateResponse : ApiResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public AuthenticateResponseUser User { get; set; } = new AuthenticateResponseUser();

        public AuthenticateResponse(Tokens tokens, string name, string surname, string username, string email)
        {
            AccessToken = tokens.AccessToken;
            RefreshToken = tokens.RefreshToken;
            User.Name = name;
            User.Surname = surname;
            User.Username = username;
            User.Email = email;
        }

        public AuthenticateResponse(Tokens tokens)
        {
            AccessToken = tokens.AccessToken;
            RefreshToken = tokens.RefreshToken;
        }

        public class AuthenticateResponseUser
        {
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }
        }
    }
}