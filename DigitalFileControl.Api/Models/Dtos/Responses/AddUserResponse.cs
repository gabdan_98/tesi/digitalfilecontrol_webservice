﻿namespace DigitalFileControl.Api.Models.Dtos.Responses
{
    public class AddUserResponse
    {
        public Guid Id { get; set; }

        public AddUserResponse(Guid id)
        {
            Id = id;
        }
    }
}
