﻿using DigitalFileControl.Model.Models;

namespace DigitalFileControl.Api.Models.Dtos.Responses
{
    #nullable disable
    public class GetDocumentDetailsResponse
    {
        public DocumentModel Document { get; set; }
        public List<UserDocumentModel> UserDocuments { get; set; }
        public List<User> UnauthorizedUsers { get; set; }

        public GetDocumentDetailsResponse(DocumentDetails details)
        {
            Document = new DocumentModel
            {
                Id = details.Document.Id,
                Name = details.Document.Name,
                Description = details.Document.Description,
                DocumentTypeId = details.Document.DocumentTypeId,
                CreationTime = details.Document.CreationTime != null ? ((DateTime)details.Document.CreationTime).ToString("yyyy-MM-dd") : null,
                ExpirationTime = details.Document.ExpirationTime != null ? ((DateTime)details.Document.ExpirationTime).ToString("yyyy-MM-dd") : null
            };

            UserDocuments = details.Document.UserDocuments
                .Where(x => !x.IsDeleted)
                .Select(x => new UserDocumentModel
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    Username = x.User.Username,
                    Name = x.User.Name,
                    Surname = x.User.Surname,
                    Email = x.User.Email,
                    CanView = x.CanView,
                    CanPrint = x.CanPrint,
                    CanDownload = x.CanDownload,
                    ViewsNumber = x.ViewsNumber,
                    MaxViewsNumber = x.MaxViewsNumber
                }).ToList();

            UnauthorizedUsers = details.UnauthorizedUsers;
        }
    }
}