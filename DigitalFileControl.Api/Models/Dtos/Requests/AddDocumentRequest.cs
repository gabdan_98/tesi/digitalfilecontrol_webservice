﻿namespace DigitalFileControl.Api.Models.Dtos.Requests
{
    public class AddDocumentRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DocumentExpire { get; set; }
        public long? ExpirationTime { get; set; }
        public int DocumentTypeId { get; set; }
        public string Content { get; set; }
    }
}