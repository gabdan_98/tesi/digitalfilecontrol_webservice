﻿namespace DigitalFileControl.Api.Models.Dtos.Requests
{
    public class RefreshTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}
