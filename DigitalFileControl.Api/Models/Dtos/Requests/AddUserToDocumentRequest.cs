﻿namespace DigitalFileControl.Api.Models.Dtos.Requests
{
    public class AddUserToDocumentRequest
    {
        public Guid UserId { get; set; }
        public int DocumentId { get; set; }
        public bool CanView { get; set; }
        public bool CanPrint { get; set; }
        public bool CanDownload { get; set; }
        public int? MaxViewsNumber { get; set; }
    }
}