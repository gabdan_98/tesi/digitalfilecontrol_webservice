﻿namespace DigitalFileControl.Api.Models
{
    public class UserDocumentModel
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public bool CanView { get; set; }
        public bool CanPrint { get; set; }
        public bool CanDownload { get; set; }
        public int ViewsNumber { get; set; }
        public int? MaxViewsNumber { get; set; }
    }
}
