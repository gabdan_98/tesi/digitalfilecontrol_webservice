﻿namespace DigitalFileControl.Api.Models
{
    public class SharedWithMeDocument
    {
        public int UserDocumentId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DocumentTypeId { get; set; }
        public bool CanView { get; set; }
        public bool CanPrint { get; set; }
        public bool CanDownload { get; set; }
        public string ExpirationTime { get; set; }
        public string SharedBy { get; set; }
    }
}