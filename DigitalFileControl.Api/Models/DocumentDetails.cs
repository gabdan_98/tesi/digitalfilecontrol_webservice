﻿using DigitalFileControl.Model.Models;

namespace DigitalFileControl.Api.Models
{
    public class DocumentDetails
    {
        public Document Document { get; set; }
        public List<User> UnauthorizedUsers { get; set; }
    }
}
