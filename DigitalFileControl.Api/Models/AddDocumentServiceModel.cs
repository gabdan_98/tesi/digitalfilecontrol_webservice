﻿namespace DigitalFileControl.Api.Models
{
    public class AddDocumentServiceModel
    {
        public string PhisicalName { get; set; }
        public int DocumentId { get; set; }

        public AddDocumentServiceModel(string phisicalName, int documentId)
        {
            PhisicalName = phisicalName;
            DocumentId = documentId;
        }
    }
}
