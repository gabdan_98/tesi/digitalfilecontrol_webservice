﻿namespace DigitalFileControl.Api.Models
{
    public class MySharedDocument
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DocumentTypeId { get; set; }
        public string? ExpirationTime { get; set; }
    }
}
