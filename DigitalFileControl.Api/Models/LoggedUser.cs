﻿using DigitalFileControl.Model.Models;

namespace DigitalFileControl.Api.Models
{
    public class LoggedUser
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
    }
}