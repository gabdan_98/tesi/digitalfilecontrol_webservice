﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Models.Dtos.Responses;
using DigitalFileControl.Api.Security;
using DigitalFileControl.Api.Services.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalFileControl.Api.Controllers
{
    #nullable disable
    public class AccountController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IJwtHelper _jwtHelper;

        public AccountController(IUserService userService, IJwtHelper jwtHelper)
        {
            _userService = userService;
            _jwtHelper = jwtHelper;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest request)
        {
            try
            {
                var user = await _userService.GetUser(request.Username, request.Password);
                if(user == null)
                {
                    Response.StatusCode = StatusCodes.Status412PreconditionFailed;
                    return null;
                }

                var tokens = _jwtHelper.Authenticate(user);
                await _userService.SaveRefreshToken(user.Id, tokens.RefreshToken);

                return new AuthenticateResponse(tokens, user.Name, user.Surname, user.Username, user.Email);
            }
            catch (BruteForceException)
            {
                Response.StatusCode = StatusCodes.Status429TooManyRequests;
                return null;
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<AuthenticateResponse> RefreshToken(RefreshTokenRequest request)
        {
            try
            {
                var tokens = await _userService.RefreshToken(request.RefreshToken);
                return new AuthenticateResponse(tokens);
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }
    }
}
