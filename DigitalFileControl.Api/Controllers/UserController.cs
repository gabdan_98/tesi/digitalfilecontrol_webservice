﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Models.Dtos.Responses;
using DigitalFileControl.Api.Services.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalFileControl.Api.Controllers
{
    #nullable disable
    [Authorize]
    public class UserController : BaseApiController
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;

        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPut]
        public async Task<AddUserResponse> AddUser(AddUserRequest request)
        {
            var user = GetLoggedUser();
            try
            {
                var userId = await _userService.AddUser(request, user);
                return new AddUserResponse(userId);
            }
            catch (UserAlreadyFoundException)
            {
                Response.StatusCode = StatusCodes.Status400BadRequest;
                return null;
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }
    }
}
