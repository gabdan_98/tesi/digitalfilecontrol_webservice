﻿using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Services.Error;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Claims;

namespace DigitalFileControl.Api.Controllers
{
    #nullable disable
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BaseApiController : ControllerBase
    {
        protected LoggedUser GetLoggedUser()
        {
            var userClaims = (User.FindFirst(ClaimTypes.UserData));
            if(userClaims == null)
            {
                return null;
            }
            return JsonConvert.DeserializeObject<LoggedUser>(userClaims.Value);
        }

        protected void LogApplicationError(Exception ex)
        {
            var errorService = HttpContext.RequestServices.GetService<IErrorService>();
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;
            errorService.LogError(controllerName, actionName, ex, GetLoggedUser());
        }
    }
}