using DigitalFileControl.Api.Controllers;
using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Helpers;
using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Models.Dtos.Response;
using DigitalFileControl.Api.Models.Dtos.Responses;
using DigitalFileControl.Api.Security;
using DigitalFileControl.Api.Services;
using DigitalFileControl.Api.Services.Document;
using DigitalFileControl.Api.Services.UserDocument;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalFileControl.Api.Controllers
{
    #nullable disable
    [Authorize]
    public class DocumentController : BaseApiController
    {
        private readonly ILogger<DocumentController> _logger;
        private readonly IDocumentService _documentService;
        private readonly IUserDocumentService _userDocumentService;
        private readonly IStorageHelper _storageHelper;
        private readonly EncryptionHelper _encryptionHelper;

        public DocumentController(ILogger<DocumentController> logger, IDocumentService documentService, IStorageHelper storageHelper, EncryptionHelper encryptionHelper, IUserDocumentService userDocumentService)
        {
            _logger = logger;
            _documentService = documentService;
            _storageHelper = storageHelper;
            _encryptionHelper = encryptionHelper;
            _userDocumentService = userDocumentService;
        }

        [HttpGet]
        public async Task<GetMySharedDocumentsResponse> GetMySharedDocuments()
        {
            var user = GetLoggedUser();
            try
            {
                return new GetMySharedDocumentsResponse(await _documentService.GetMySharedDocuments(user));
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpGet]
        public async Task<GetSharedWithMeDocumentsResponse> GetSharedWithMeDocuments()
        {
            var user = GetLoggedUser();
            try
            {
                return new GetSharedWithMeDocumentsResponse(await _documentService.GetSharedWithMeDocuments(user));
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [ViewerSecurity]
        [HttpGet("{id}")]
        public async Task<GetDocumentResponse> GetDocument(int id)
        {
            var user = GetLoggedUser();
            try
            {
                var document = await _documentService.GetDocument(id, user);
                if (document == null)
                {
                    Response.StatusCode = StatusCodes.Status404NotFound;
                    return null;
                }

                var documentContent = await _storageHelper.GetDocumentBase64(document.PhisicalName);
                document.Content = _encryptionHelper.Encrypt(documentContent);
                return new GetDocumentResponse(document);
            }
            catch (ViewsMaxNumberExceededException)
            {
                Response.StatusCode = StatusCodes.Status412PreconditionFailed;
                return null;
            }
            catch (DocumentViewDisabledException)
            {
                Response.StatusCode = StatusCodes.Status403Forbidden;
                return null;
            }
            catch (ExpiredDocumentException)
            {
                Response.StatusCode = StatusCodes.Status423Locked;
                return null;
            }
            catch(Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpGet("{id}")]
        public async Task<GetDocumentDetailsResponse> GetDocumentDetails(int id)
        {
            var user = GetLoggedUser();
            try
            {
                var documents = await _userDocumentService.GetDocumentDetail(id, user.Id);
                return new GetDocumentDetailsResponse(documents);
            }
            catch (NotFoundException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
                return null;
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpPut]
        public async Task<AddDocumentResponse> AddDocument(AddDocumentRequest request)
        {
            var user = GetLoggedUser();
            try
            {
                var addDocumentModel = await _documentService.AddDocument(request, user);
                await _storageHelper.UploadDocument(request.Content, addDocumentModel.PhisicalName);
                return new AddDocumentResponse(addDocumentModel.DocumentId);
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDocument(int id)
        {
            try
            {
                var user = GetLoggedUser();
                var phisicalName = await _documentService.DeleteDocument(id, user);
                await _storageHelper.DeleteDocument(phisicalName);
                return Ok();
            }
            catch (ForbiddenException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }
    }
}