﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Models.Dtos.Responses;
using DigitalFileControl.Api.Services.UserDocument;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalFileControl.Api.Controllers
{
    [Authorize]
    public class UserDocumentController : BaseApiController
    {
        private readonly IUserDocumentService _userDocumentService;

        public UserDocumentController(IUserDocumentService userDocumentService)
        {
            _userDocumentService = userDocumentService;
        }

        [HttpPut]
        public async Task<AddUserToDocumentResponse> AddUserToDocument(AddUserToDocumentRequest request)
        {
            var user = GetLoggedUser();
            try
            {
                var userDocumentId = await _userDocumentService.AddUserToDocument(request, user.Id);
                return new AddUserToDocumentResponse(userDocumentId);
            }
            catch (NotFoundException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
                return null;
            }
            catch (Exception ex)
            {
                LogApplicationError(ex);
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return null;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserFromDocument(int id)
        {
            var user = GetLoggedUser();
            try
            {
                await _userDocumentService.DeleteUserFromDocument(id, user.Id);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ForbiddenException)
            {
                return Forbid();
            }
            catch(Exception ex)
            {
                LogApplicationError(ex);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}