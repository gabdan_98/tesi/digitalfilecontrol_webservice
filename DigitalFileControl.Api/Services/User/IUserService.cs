﻿using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Model.Models;

namespace DigitalFileControl.Api.Services.User
{
    public interface IUserService
    {
        Task<Guid> AddUser(AddUserRequest request, LoggedUser loggedUser);
        Task<LoggedUser?> GetUser(string username, string password);
        Task<LoggedUser?> GetUserByUsernameOrEmail(string username, string email);
        Task<UserModel?> GetUser(Guid Id);
        Task SaveRefreshToken(Guid userId, string refreshToken);
        Task<Tokens> RefreshToken(string refreshToken);
        Task<List<Model.Models.User>> GetUserExceptTheseAndMe(List<Guid> idsToExclude, Guid loggedUserId);
    }
}