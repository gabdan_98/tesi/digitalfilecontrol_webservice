﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Security;
using DigitalFileControl.Model;
using DigitalFileControl.Model.Models;
using Microsoft.EntityFrameworkCore;
using BCryptNet = BCrypt.Net.BCrypt;

namespace DigitalFileControl.Api.Services.User
{
    #nullable disable
    public class UserServiceSQL : IUserService
    {
        private readonly DigitalFileControlDB _dbContext;
        private readonly IJwtHelper _jwtHelper;
        private readonly IConfiguration _configuration;

        public UserServiceSQL(DigitalFileControlDB dbContext, IJwtHelper jwthelper, IConfiguration configutation)
        {
            _dbContext = dbContext;
            _jwtHelper = jwthelper;
            _configuration = configutation;
        }

        public async Task<Guid> AddUser(AddUserRequest request, LoggedUser loggedUser)
        {
            var user = await GetUserByUsernameOrEmail(request.Username, request.Email);
            if (user != null)
            {
                throw new UserAlreadyFoundException();
            }

            var userToAdd = new Model.Models.User
            {
                Name = request.Name,
                Surname = request.Surname,
                Username = request.Username,
                Password = BCryptNet.HashPassword(request.Password),
                Email = request.Email,
                CreationTime = DateTime.Now,
                CreatorUserId = loggedUser.Id.ToString(),
                IsActive = true,
                IsDeleted = false
            };

            await _dbContext.Users.AddAsync(userToAdd);

            await _dbContext.SaveChangesAsync();
            return userToAdd.Id;
        }

        public async Task<LoggedUser?> GetUser(string username, string password)
        {
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Username.ToLower() == username && x.IsActive);

            Guid? userId = null;
            if(user != null)
            {
                userId = user.Id;
                if (await DetectBruteForce((Guid)userId))
                {
                    await AddLoginLog(username, userId, false);
                    throw new BruteForceException();
                }
            }
            
            if(user == null)
            {
                await AddLoginLog(username, userId, false);
                return null;
            }

            if(BCryptNet.Verify(password, user.Password))
            {
                await AddLoginLog(username, userId, true);
                return new LoggedUser
                {
                    Id = user.Id,
                    Username = user.Username,
                    Name = user.Name,
                    Surname = user.Surname,
                    Email = user.Email
                };
            }

            await AddLoginLog(username, userId, false);
            return null;
        }

        private async Task<bool> DetectBruteForce(Guid userId)
        {
            var startDateTime = DateTime.Now.AddHours(-3);

            var result = await _dbContext.LoginLogs
                .Where(x => x.UserId == userId && x.LoginTime > startDateTime)
                .OrderByDescending(x => x.Id)
                .Take(6)
                .ToListAsync();

            var lastLogins = result.Take(5);
            if(lastLogins.Count() >= 5 && lastLogins.All(x => !x.LoginSuccess))
            {
                return true;
            }
            return false;
        }

        private async Task AddLoginLog(string username, Guid? userId, bool success)
        {
            await _dbContext.LoginLogs.AddAsync(new LoginLog
            {
                Username = username,
                UserId = userId,
                LoginTime = DateTime.Now,
                LoginSuccess = success
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task<LoggedUser?> GetUserByUsernameOrEmail(string username, string email)
        {
            return await _dbContext.Users
                .Where(x => x.Username.ToLower() == username || x.Email.ToLower() == email)
                .Select(x => new LoggedUser
                {
                    Id = x.Id,
                    Username = x.Username
                })
                .FirstOrDefaultAsync();
        }

        public async Task<UserModel?> GetUser(Guid Id)
        {
            return await _dbContext.Users
                .Include(x => x.UserDocuments)
                .Include(x => x.OwnedDocuments)
                .Select(x => new UserModel
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    Username = x.Username,
                    UserDocuments = x.UserDocuments,
                    OwnedDocuments = x.OwnedDocuments
                }).FirstOrDefaultAsync();
        }

        public async Task<List<Model.Models.User>> GetUserExceptTheseAndMe(List<Guid> idsToExclude, Guid loggedUserId)
        {
            return await _dbContext.Users
                .Where(x => !idsToExclude.Any(y => y == x.Id) && x.Id != loggedUserId)
                .ToListAsync();
        }

        public async Task<Tokens> RefreshToken(string refreshToken)
        {
            var user = await _dbContext.Users
                .Include(x => x.RefreshTokens)
                .FirstOrDefaultAsync(x => x.RefreshTokens.Any(y => y.Token == refreshToken));

            if(user == null)
            {
                throw new NotFoundException();
            }

            var refreshTokenObject = user.RefreshTokens.FirstOrDefault(x => x.Token == refreshToken);
            if(!refreshTokenObject.IsActive)
            {
                throw new NotFoundException();
            }

            var newRefValue = _jwtHelper.GenerateRefreshToken();
            refreshTokenObject.RevocationTime = DateTime.UtcNow;
            refreshTokenObject.ReplacedByToken = newRefValue;

            var newRefreshToken = new RefreshToken
            {
                Token = newRefValue,
                ExpirationTime = DateTime.Now.AddDays(Int32.Parse(_configuration.GetSection("JWTAuth:RefreshTokenDuration").Value)),
                CreationTime = DateTime.Now
            };
            user.RefreshTokens.Add(newRefreshToken);

            _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();

            var response = _jwtHelper.Authenticate(new LoggedUser
            {
                Id = user.Id,
                Username = user.Username
            });

            response.RefreshToken = newRefValue;
            return response;
        }

        public async Task SaveRefreshToken(Guid userId, string refreshToken)
        {
            await _dbContext.RefreshTokens.AddAsync(new RefreshToken
            {
                Token = refreshToken,
                ExpirationTime = DateTime.Now.AddDays(Int32.Parse(_configuration.GetSection("JWTAuth:RefreshTokenDuration").Value)),
                CreationTime = DateTime.Now,
                UserId = userId
            });

            await _dbContext.SaveChangesAsync();
        }
    }
}