﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Services.User;
using DigitalFileControl.Model;
using DigitalFileControl.Model.Enums;
using DigitalFileControl.Model.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace DigitalFileControl.Api.Services.Document
{
    #nullable disable
    public class DocumentServiceSQL : IDocumentService
    {
        private readonly DigitalFileControlDB _dbContext;

        public DocumentServiceSQL(DigitalFileControlDB dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<SharedWithMeDocument>> GetSharedWithMeDocuments(LoggedUser loggedUser)
        {
            return await _dbContext.UserDocuments
                .Include(x => x.Document).ThenInclude(x => x.Owner)
                .Where(x => x.UserId == loggedUser.Id && x.IsVisible && !x.Document.IsDeleted && !x.IsDeleted)
                .Select(x => new SharedWithMeDocument
                {
                    Id = x.DocumentId,
                    UserDocumentId = x.Id,
                    Name = x.Document.Name,
                    Description = x.Document.Description,
                    DocumentTypeId = x.Document.DocumentTypeId,
                    ExpirationTime = x.Document.ExpirationTime != null ? ((DateTime)x.Document.ExpirationTime).ToString("yyyy-MM-dd") : null,
                    CanView = x.CanView,
                    CanPrint = x.CanPrint,
                    CanDownload = x.CanDownload,
                    SharedBy = x.Document.Owner.Name + " " + x.Document.Owner.Surname
                }).ToListAsync();
        }

        public async Task<List<MySharedDocument>> GetMySharedDocuments(LoggedUser loggedUser)
        {
            return await _dbContext.Documents
                .Where(x => x.OwnerId == loggedUser.Id && !x.IsDeleted)
                .Select(x => new MySharedDocument
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    DocumentTypeId = x.DocumentTypeId,
                    ExpirationTime = x.ExpirationTime != null ? ((DateTime)x.ExpirationTime).ToString("yyyy-MM-dd") : null,
                })
                .ToListAsync();
        }

        public async Task<DocumentModel?> GetDocument(int id, LoggedUser loggedUser)
        {
            await _dbContext.Database.OpenConnectionAsync();
            var con = (SqlConnection)_dbContext.Database.GetDbConnection();
            var cmd = new SqlCommand($"EXEC GetDocument @UserId = '{loggedUser.Id}', @DocumentId = {id}", con);
            DataTable dt = new DataTable();
            using (var reader = await cmd.ExecuteReaderAsync())
            {
                dt.Load(reader);
            }

            if(dt.Rows.Count == 0)
            {
                return null;
            }

            var result = dt.Rows[0];
            int? viewsNumber = !string.IsNullOrEmpty(result["ViewsNumber"].ToString()) ? (int)result["ViewsNumber"] : null;
            int? maxViewsNumber = !string.IsNullOrEmpty(result["MaxViewsNumber"].ToString()) ? (int)result["MaxViewsNumber"] : null;

            if(!(bool)result["CanView"])
            {
                throw new DocumentViewDisabledException();
            }

            if(viewsNumber != null && maxViewsNumber != null && viewsNumber == maxViewsNumber)
            {
                throw new ViewsMaxNumberExceededException();
            }

            string expirationTime = null;
            Boolean exceptionToThrow = false;

            try
            {
                if ((DateTime)result["ExpirationTime"] < DateTime.Now)
                {
                    exceptionToThrow = true;
                }
                expirationTime = ((DateTime)result["ExpirationTime"]).ToString("yyyy-MM-dd");
            }
            catch
            {

            }

            if(exceptionToThrow)
            {
                throw new ExpiredDocumentException();
            }

            return new DocumentModel
            {
                Id = (int)result["DocumentId"],
                UserDocumentId = !string.IsNullOrEmpty(result["Id"].ToString()) ? (int)result["Id"] : null,
                Name = result["Name"].ToString(),
                Description = result["Description"].ToString(),
                PhisicalName = result["PhisicalName"].ToString(),
                DocumentTypeId = (int)result["DocumentTypeId"],
                CanView = (bool)result["CanView"],
                CanPrint = (bool)result["CanPrint"],
                CanDownload = (bool)result["CanDownload"],
                ExpirationTime = expirationTime,
                OwnerId = (Guid?)result["OwnerId"]
            };
        }

        public async Task<AddDocumentServiceModel> AddDocument(AddDocumentRequest request, LoggedUser loggedUser)
        {
            var phisicalName = Guid.NewGuid().ToString();
            DateTime beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var documentToAdd = new Model.Models.Document
            {
                Name = request.Name,
                Description = request.Description,
                DocumentTypeId = request.DocumentTypeId,
                ExpirationTime = request.DocumentExpire ? beginTicks.AddMilliseconds((long)request.ExpirationTime).AddHours(2) : null,
                CreationTime = DateTime.Now,
                CreatorUserId = loggedUser.Id.ToString(),
                IsDeleted = false,
                PhisicalName = phisicalName,
                OwnerId = loggedUser.Id
            };

            await _dbContext.Documents.AddAsync(documentToAdd);

            await _dbContext.SaveChangesAsync();

            return new AddDocumentServiceModel(phisicalName, documentToAdd.Id);
        }

        public async Task<string> DeleteDocument(int id, LoggedUser loggedUser)
        {
            var documentToRemove = await _dbContext.Documents.FirstOrDefaultAsync(x => x.Id == id);
            if(documentToRemove.OwnerId != loggedUser.Id)
            {
                throw new ForbiddenException();
            }

            var phisicalName = documentToRemove.PhisicalName;
            documentToRemove.IsDeleted = true;
            documentToRemove.DeletionTime = DateTime.Now;
            documentToRemove.DeletorUserId = loggedUser.Id.ToString();
            await _dbContext.SaveChangesAsync();
            return phisicalName;
        }
    }
}
