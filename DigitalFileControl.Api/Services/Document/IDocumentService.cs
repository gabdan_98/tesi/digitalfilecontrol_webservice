﻿using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;

namespace DigitalFileControl.Api.Services.Document
{
    public interface IDocumentService
    {
        Task<List<MySharedDocument>> GetMySharedDocuments(LoggedUser loggedUser);
        Task<List<SharedWithMeDocument>> GetSharedWithMeDocuments(LoggedUser loggedUser);
        Task<Models.DocumentModel?> GetDocument(int id, LoggedUser loggedUser);
        Task<AddDocumentServiceModel> AddDocument(AddDocumentRequest request, LoggedUser loggedUser);
        Task<string> DeleteDocument(int id, LoggedUser loggedUser);
    }
}
