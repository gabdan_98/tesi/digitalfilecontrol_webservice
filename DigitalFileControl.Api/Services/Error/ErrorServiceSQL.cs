﻿using DigitalFileControl.Api.Models;
using DigitalFileControl.Model;

namespace DigitalFileControl.Api.Services.Error
{
    #nullable disable
    public class ErrorServiceSQL : IErrorService
    {
        public async Task LogError(string controllerName, string actionName, Exception ex, LoggedUser loggedUser)
        {
            using (var dbContext = new DigitalFileControlDB())
            {
                await dbContext.ErrorLogs.AddAsync(new Model.Models.ErrorLog
                {
                    Controller = controllerName,
                    Action = actionName,
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace,
                    UserId = loggedUser != null ? loggedUser.Id.ToString() : null,
                    ErrorTime = DateTime.Now
                });

                await dbContext.SaveChangesAsync();
            }
        }
    }
}