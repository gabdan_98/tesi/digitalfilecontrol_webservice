﻿using DigitalFileControl.Api.Models;

namespace DigitalFileControl.Api.Services.Error
{
    public interface IErrorService
    {
        Task LogError(string controllerName, string actionName, Exception ex, LoggedUser loggedUser);
    }
}
