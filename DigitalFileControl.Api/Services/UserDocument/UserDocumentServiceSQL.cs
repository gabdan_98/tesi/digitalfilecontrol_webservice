﻿using DigitalFileControl.Api.Exceptions;
using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;
using DigitalFileControl.Api.Services.User;
using DigitalFileControl.Model;
using Microsoft.EntityFrameworkCore;

namespace DigitalFileControl.Api.Services.UserDocument
{
    #nullable disable
    public class UserDocumentServiceSQL : IUserDocumentService
    {
        private readonly DigitalFileControlDB _dbContext;
        private readonly IUserService _userService;

        public UserDocumentServiceSQL(DigitalFileControlDB dbContext, IUserService userSerice)
        {
            _dbContext = dbContext;
            _userService = userSerice;
        }

        public async Task<int> AddUserToDocument(AddUserToDocumentRequest request, Guid userId)
        {
            var document = await _dbContext.Documents
                .Where(x => x.Id == request.DocumentId && x.OwnerId == userId && !x.IsDeleted)
                .FirstOrDefaultAsync();

            if(document == null)
            {
                throw new NotFoundException();
            }

            var userDocument = new Model.Models.UserDocument
            {
                UserId = request.UserId,
                DocumentId = request.DocumentId,
                IsVisible = true,
                CanView = request.CanView,
                CanPrint = request.CanPrint,
                CanDownload = request.CanDownload,
                ViewsNumber = 0,
                MaxViewsNumber = request.MaxViewsNumber,
                CreationTime = DateTime.Now,
                CreatorUserId = userId.ToString(),
                IsDeleted = false
            };

            await _dbContext.UserDocuments.AddAsync(userDocument);

            await _dbContext.SaveChangesAsync();
            return userDocument.Id;
        }

        public async Task DeleteUserFromDocument(int id, Guid userId)
        {
            var userDocument = await _dbContext.UserDocuments
                .Include(x => x.Document)
                .FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);
            if(userDocument == null)
            {
                throw new NotFoundException();
            }

            if(userDocument.Document.OwnerId != userId)
            {
                throw new ForbiddenException();
            }

            userDocument.IsDeleted = true;
            userDocument.DeletionTime = DateTime.Now;
            userDocument.DeletorUserId = userId.ToString();

            await _dbContext.SaveChangesAsync();
        }

        public async Task<DocumentDetails> GetDocumentDetail(int documentId, Guid userId)
        {
            var document = await _dbContext.Documents
                .Include(x => x.UserDocuments).ThenInclude(x => x.User)
                .Where(x => x.Id == documentId && !x.IsDeleted && x.OwnerId == userId)
                .FirstOrDefaultAsync();

            if(document == null)
            {
                throw new NotFoundException();
            }

            var userToExcludeIds = document.UserDocuments
                .Where(x => !x.IsDeleted || x.UserId == userId)
                .Select(x => x.UserId)
                .ToList();

            var unauthorizedUsers = await _userService.GetUserExceptTheseAndMe(userToExcludeIds, userId);

            return new DocumentDetails { Document = document, UnauthorizedUsers = unauthorizedUsers };
        }
    }
}