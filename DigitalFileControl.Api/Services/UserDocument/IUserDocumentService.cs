﻿using DigitalFileControl.Api.Models;
using DigitalFileControl.Api.Models.Dtos.Requests;

namespace DigitalFileControl.Api.Services.UserDocument
{
    public interface IUserDocumentService
    {
        Task<int> AddUserToDocument(AddUserToDocumentRequest request, Guid userId);
        Task<DocumentDetails> GetDocumentDetail(int documentId, Guid userId);
        Task DeleteUserFromDocument(int id, Guid userId);
    }
}