﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace DigitalFileControl.Api.Helpers
{
    public class AzureBlobStorageHelper : IStorageHelper
    {
        private readonly IConfiguration _configuration;

        public AzureBlobStorageHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> GetDocumentBase64(string documentName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configuration.GetSection("AzureBlobStorage:ConnectionString").Value);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(_configuration.GetSection("AzureBlobStorage:ContainerName").Value);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(documentName);
            var b64String = string.Empty;
            using (var memoryStream = new MemoryStream())
            {
                await blockBlob.DownloadToStreamAsync(memoryStream);
                var bytes = memoryStream.ToArray();
                b64String = Convert.ToBase64String(bytes);
            }
            return b64String;
        }

        public async Task UploadDocument(string base64, string documentName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configuration.GetSection("AzureBlobStorage:ConnectionString").Value);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(_configuration.GetSection("AzureBlobStorage:ContainerName").Value);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(documentName);

            var bytes = Convert.FromBase64String(base64);
            using (var stream = new MemoryStream(bytes))
            {
                await blockBlob.UploadFromStreamAsync(stream);
            }
        }

        public async Task DeleteDocument(string documentName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configuration.GetSection("AzureBlobStorage:ConnectionString").Value);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(_configuration.GetSection("AzureBlobStorage:ContainerName").Value);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(documentName);
            await blockBlob.DeleteAsync();
        }
    }
}
