﻿namespace DigitalFileControl.Api.Helpers
{
    public interface IStorageHelper
    {
        Task<string> GetDocumentBase64(string documentName);
        Task UploadDocument(string base64, string documentName);
        Task DeleteDocument(string documentName);
    }
}