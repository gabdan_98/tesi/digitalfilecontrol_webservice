﻿using System.Security.Cryptography;
using System.Text;

namespace DigitalFileControl.Api.Helpers
{
    public class EncryptionHelper
    {
        public EncryptionHelper()
        {
            
        }
        private static byte[] salt = new byte[] { 34, 134, 145, 12, 7, 6, 243, 63, 43, 54, 75, 65, 53, 2, 34, 54, 45, 67, 64, 64, 32, 213 };
        private static byte[] vector = Encoding.ASCII.GetBytes("16806642kbM7c5!$");
        private static string key = "I2?_S*/SG2sg!+sèS24@sfe3Tt£t23h_S-&%DG2?";

        public string Encrypt(string plainText)
        {
            var kdf = new Rfc2898DeriveBytes(key, salt);
            using (var aes = Aes.Create())
            {
                aes.Key = kdf.GetBytes(aes.KeySize / 8);
                aes.IV = vector;
                using (var encryptor = aes.CreateEncryptor(aes.Key, aes.IV))
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        // don't use StreamWriter, it just makes things more complicated
                        var bytes = Encoding.ASCII.GetBytes(plainText);
                        cs.Write(bytes, 0, bytes.Length);
                    }
                    return Convert.ToBase64String(ms.ToArray());
                }
            }
        }
    }
}
