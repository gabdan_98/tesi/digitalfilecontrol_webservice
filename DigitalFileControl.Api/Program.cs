using DigitalFileControl.Api.Filters;
using DigitalFileControl.Api.Helpers;
using DigitalFileControl.Api.Security;
using DigitalFileControl.Api.Services;
using DigitalFileControl.Api.Services.Document;
using DigitalFileControl.Api.Services.Error;
using DigitalFileControl.Api.Services.User;
using DigitalFileControl.Api.Services.UserDocument;
using DigitalFileControl.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
ConfigureServices(builder.Services);

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

var option = new RewriteOptions();
option.AddRedirect("^$", "swagger");
app.UseRewriter(option);

app.Run();

void ConfigureServices(IServiceCollection services)
{
    services.AddControllers()
        .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

    services.AddEndpointsApiExplorer();

    services.AddSwaggerGen(c =>
    {
        // Configure Swagger
        // "Bearer" is the name for this definition. Any other name could be used
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please enter a valid token",
            Name = "Authorization",
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            Scheme = "Bearer"
        });

        c.OperationFilter<SwaggerAuthorize>();
    });

    services.AddDbContext<DigitalFileControlDB>(options =>
            options.UseSqlServer(configuration.GetConnectionString("DigitalFileControlDB")));

    services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(option =>
    {
        var Key = Encoding.UTF8.GetBytes(configuration["JWTAuth:Key"]);
        option.SaveToken = true;
        option.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration["JWTAuth:Issuer"],
            ValidAudience = configuration["JWTAuth:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Key)
        };
    });

    services.AddSingleton<IJwtHelper, JwtHelper>();

    services.AddTransient<IDocumentService, DocumentServiceSQL>();
    services.AddTransient<IUserService, UserServiceSQL>();
    services.AddTransient<IUserDocumentService, UserDocumentServiceSQL>();
    services.AddTransient<IErrorService, ErrorServiceSQL>();
    services.AddTransient<IStorageHelper, AzureBlobStorageHelper>();
    services.AddTransient<EncryptionHelper>();
}