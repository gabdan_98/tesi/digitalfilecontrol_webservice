﻿using BCrypt.Net;
using DigitalFileControl.Model.Enums;
using DigitalFileControl.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DocumentType>().HasData(
                new DocumentType { Id = 1, Name = "PDF" }
            );

            modelBuilder.Entity<User>().HasData(
                new User {
                    Id = Guid.Parse("992BE209-A765-47BD-1058-08DA257A523C"),
                    Name = "Gabriele",
                    Surname = "D'Andrea",
                    Email = "gabriele.dandrea001@studenti.unibas.it",
                    Username = "gabriele.dandrea",
                    Password = BCrypt.Net.BCrypt.HashPassword("Password_00"),
                    IsActive = true,
                    IsDeleted = false,
                    CreationTime = DateTime.Now
                },
                new User
                {
                    Id = Guid.Parse("8AC1FDBF-D60B-4519-8C20-08DA257F073F"),
                    Name = "Domenico",
                    Surname = "Bloisi",
                    Email = "domenico.bloisi@unibas.it",
                    Username = "domenico.bloisi",
                    Password = BCrypt.Net.BCrypt.HashPassword("Password_00"),
                    IsActive = true,
                    IsDeleted = false,
                    CreationTime = DateTime.Now
                }
            );

            modelBuilder.Entity<Document>().HasData(
                new Document
                {
                    Id = 1,
                    Name = "Delega",
                    Description = "Documento generico di delega",
                    DocumentTypeId = (int)DocumentTypes.PDF,
                    ExpirationTime = DateTime.Now.AddYears(1),
                    CreationTime = DateTime.Now,
                    IsDeleted = false,
                    PhisicalName = "delega.pdf",
                    OwnerId = Guid.Parse("992BE209-A765-47BD-1058-08DA257A523C")
                },
                new Document
                {
                    Id = 2,
                    Name = "Privacy",
                    Description = "Modello per la protezione dei dati personali",
                    DocumentTypeId = (int)DocumentTypes.PDF,
                    ExpirationTime = DateTime.Now.AddYears(1),
                    CreationTime = DateTime.Now,
                    IsDeleted = false,
                    PhisicalName = "privacy.pdf",
                    OwnerId = Guid.Parse("992BE209-A765-47BD-1058-08DA257A523C")
                },
                new Document
                {
                    Id = 3,
                    Name = "CUD",
                    Description = "Modello certificazione unica",
                    DocumentTypeId = (int)DocumentTypes.PDF,
                    ExpirationTime = DateTime.Now.AddYears(1),
                    CreationTime = DateTime.Now,
                    IsDeleted = false,
                    PhisicalName = "cud.pdf",
                    OwnerId = Guid.Parse("8AC1FDBF-D60B-4519-8C20-08DA257F073F")
                }
            );

            modelBuilder.Entity<UserDocument>().HasData(
                new UserDocument
                {
                    Id = 1,
                    UserId = Guid.Parse("8AC1FDBF-D60B-4519-8C20-08DA257F073F"),
                    DocumentId = 1,
                    IsVisible = true,
                    CanView = true,
                    CanPrint = false,
                    CanDownload = false,
                    ViewsNumber = 0
                },
                new UserDocument
                {
                    Id = 2,
                    UserId = Guid.Parse("8AC1FDBF-D60B-4519-8C20-08DA257F073F"),
                    DocumentId = 2,
                    IsVisible = true,
                    CanView = true,
                    CanPrint = false,
                    CanDownload = false,
                    ViewsNumber = 0,
                    MaxViewsNumber = 5
                },
                new UserDocument
                {
                    Id = 3,
                    UserId = Guid.Parse("992BE209-A765-47BD-1058-08DA257A523C"),
                    DocumentId = 3,
                    IsVisible = true,
                    CanView = true,
                    CanPrint = false,
                    CanDownload = false,
                    ViewsNumber = 0,
                    MaxViewsNumber = 5
                }
            );
        }
    }
}
