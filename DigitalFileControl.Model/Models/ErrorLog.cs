﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class ErrorLog
    {
        public int Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string ErrorMessage { get; set; }
        public string StackTrace { get; set; }
        public string? UserId { get; set; }
        public DateTime ErrorTime { get; set; }
    }
}