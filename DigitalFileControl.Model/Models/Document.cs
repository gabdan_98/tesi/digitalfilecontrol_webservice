﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalFileControl.Model.Models
{
    public class Document : Base
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhisicalName { get; set; }
        public int DocumentTypeId { get; set; }
        public DateTime? ExpirationTime { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? OwnerId { get; set; }

        // Navigation properties
        public DocumentType DocumentType { get; set; }
        public User? Owner { get; set; }
        public List<UserDocument> UserDocuments { get; set; }
    }
}