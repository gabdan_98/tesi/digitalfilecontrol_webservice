﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class UserDocument : Base
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int DocumentId { get; set; }
        public bool IsVisible { get; set; }
        public bool CanView { get; set; }
        public bool CanPrint { get; set; }
        public bool CanDownload { get; set; }
        public int ViewsNumber { get; set; }
        public int? MaxViewsNumber { get; set; }
        public bool IsDeleted { get; set; }

        // Navigation properties
        public User User { get; set; }
        public Document Document { get; set; }
    }
}