﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class ViewLogs
    {
        public int Id { get; set; }
        public int DocumentId { get; set; }
        public Guid UserId { get; set; }
        public DateTime ViewTime { get; set; }

        // Navigation properties
        public Document Document { get; set; }
        public User User { get; set; }
    }
}