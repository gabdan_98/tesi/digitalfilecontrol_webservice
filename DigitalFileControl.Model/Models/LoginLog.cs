﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class LoginLog
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public Guid? UserId { get; set; }
        public bool LoginSuccess { get; set; }
        public DateTime LoginTime { get; set; }
    }
}