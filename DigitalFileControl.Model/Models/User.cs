﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class User : Base
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        // Navigation properties
        public List<UserDocument> UserDocuments { get; set; }
        public List<Document> OwnedDocuments { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; }
    }
}