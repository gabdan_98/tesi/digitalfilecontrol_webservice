﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class Base
    {
        public DateTime CreationTime { get; set; }
        public string? CreatorUserId { get; set; }
        public DateTime? ModificationTime { get; set; }
        public string? ModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public string? DeletorUserId { get; set; }
    }
}