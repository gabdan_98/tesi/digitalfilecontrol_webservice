﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalFileControl.Model.Models
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime ExpirationTime { get; set; }
        public bool IsExpired { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? RevocationTime { get; set; }
        public string? ReplacedByToken { get; set; }
        public bool IsActive => RevocationTime == null && !IsExpired;
        public Guid UserId { get; set; }

        // Navigation properties
        public User User { get; set; }
    }
}