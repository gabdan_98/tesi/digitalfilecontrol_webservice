﻿using System;
using System.Collections.Generic;
using DigitalFileControl.Model.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DigitalFileControl.Model
{
    public class DigitalFileControlDB : DbContext
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

        public DbSet<Document> Documents => Set<Document>();
        public DbSet<DocumentType> DocumentTypes => Set<DocumentType>();
        public DbSet<User> Users => Set<User>();
        public DbSet<UserDocument> UserDocuments => Set<UserDocument>();
        public DbSet<ErrorLog> ErrorLogs => Set<ErrorLog>();
        public DbSet<ViewLogs> ViewLogs => Set<ViewLogs>();
        public DbSet<RefreshToken> RefreshTokens => Set<RefreshToken>();
        public DbSet<LoginLog> LoginLogs => Set<LoginLog>();


        public DigitalFileControlDB() { }
        public DigitalFileControlDB(DbContextOptions<DigitalFileControlDB> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(configuration.GetConnectionString("DigitalFileControlDB"));
    }
}