﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Added_entity_LoginLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoginLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LoginSuccess = table.Column<bool>(type: "bit", nullable: false),
                    LoginTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginLogs", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(19), new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(10) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(49), new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(47) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(147), new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(132) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 6, 3, 18, 15, 47, 191, DateTimeKind.Local).AddTicks(9594), "$2a$11$TqpkPV/bVaFTNT3UkOwqOuob/4.SAuTrBCg72EIPTW5bdrqHkq5Qi" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 6, 3, 18, 15, 47, 73, DateTimeKind.Local).AddTicks(1430), "$2a$11$o7DacwNTYMP.l3NMDdhnse7jl3DgkbI8tR7Y4xeaK4biAm.uoCDp." });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginLogs");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1045), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1035) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1064), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1061) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1156), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1141) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(648), "$2a$11$6Y4ah7zeCPRJhPM9tzcz0.V67oUVQBRwRdwBYUjJJIt/bbSZxMO.a" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 532, DateTimeKind.Local).AddTicks(309), "$2a$11$/wZMm9PtNEMHW4L4HV9DOekwGXtwBdxHR09c0D7w22HJ8hti0yp9i" });
        }
    }
}
