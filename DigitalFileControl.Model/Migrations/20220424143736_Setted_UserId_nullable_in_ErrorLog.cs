﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Setted_UserId_nullable_in_ErrorLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "ErrorLogs",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8439), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8431) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8463), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8461) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8541), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8529) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8100), "$2a$11$AqxymqwtqYhi4MNHLQnrQOjjoyXQRbzmTalEKYikjzkHtWKXVvSHu" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 487, DateTimeKind.Local).AddTicks(6353), "$2a$11$Sagq44fGoH7OG5Pu./zyDeU1Z.jEr7YeF0kDyZ7sIZznebvfdKh8." });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "ErrorLogs",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5339), new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5330) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5357), new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5355) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5407), new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5398) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5099), "$2a$11$ZbjHk4sGnBMBrs54d0l/3.oG6NU9ogybHa8Y23lGlT3TUYeHjn7bi" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 20, 57, 279, DateTimeKind.Local).AddTicks(5263), "$2a$11$IjBTbhlRRTYrU5H1zWWhIex.c.XbA72pyl/TNv7NvgASF7nDwtC/q" });
        }
    }
}
