﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Added_RefreshToken_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ExpirationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsExpired = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RevocationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReplacedByToken = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9772), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9789), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9787) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9858), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9843) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9462), "$2a$11$6A/gv7SF0yJvye3bv/n.RO/o/bYksg9XgXzCJyl3l8dQXIiCiPnfy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 598, DateTimeKind.Local).AddTicks(7507), "$2a$11$cQ..i9tVddiLXXJDWT2wwOXlrM5IZP//twep.10igq9Y5sdv3oTti" });

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7651), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7644) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7685), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7683) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7737), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7727) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7439), "$2a$11$8dhA8uuiqVyt/fcVpY8Co.7xGFChf/ZR11HZLx1EJaLDTz1Utx552" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 529, DateTimeKind.Local).AddTicks(5940), "$2a$11$LPoWc3FVUq1urLMTaMcHWeNwcdL4M5V0txGXJcdYIhjVZwvbx3Ywi" });
        }
    }
}
