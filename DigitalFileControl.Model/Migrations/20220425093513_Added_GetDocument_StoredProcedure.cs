﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Added_GetDocument_StoredProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7651), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7644) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7685), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7683) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7737), new DateTime(2023, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7727) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 643, DateTimeKind.Local).AddTicks(7439), "$2a$11$8dhA8uuiqVyt/fcVpY8Co.7xGFChf/ZR11HZLx1EJaLDTz1Utx552" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 25, 11, 35, 13, 529, DateTimeKind.Local).AddTicks(5940), "$2a$11$LPoWc3FVUq1urLMTaMcHWeNwcdL4M5V0txGXJcdYIhjVZwvbx3Ywi" });

            var sp = @"CREATE PROCEDURE GetDocument 
	                    @UserId uniqueidentifier,
	                    @DocumentId int
                    AS
                    BEGIN
	                    SET NOCOUNT ON;	
	                    DECLARE @Result TABLE (Id INT, DocumentId INT, Name VARCHAR(max), Description VARCHAR(max), PhisicalName VARCHAR(max), DocumentTypeId INT, ExpirationTime DATETIME, CanView BIT, CanPrint BIT, CanDownload BIT, OwnerId UNIQUEIDENTIFIER, ViewsNumber INT, MaxViewsNumber INT)

	                    INSERT INTO @Result
	                    SELECT NULL AS Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, 1 AS CanView, 1 AS CanPrint, 1 AS CanDownload, d.OwnerId, null AS ViewsNumber, null AS MaxViewsNumber
                        FROM Documents d
                        WHERE d.IsDeleted = 0
                        AND d.Id = @DocumentId
                        AND d.OwnerId = @UserId
                        UNION
                        SELECT ud.Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, ud.CanView, ud.CanPrint, ud.CanDownload, d.OwnerId, ud.ViewsNumber, ud.MaxViewsNumber
                        FROM Documents d INNER JOIN UserDocuments ud ON d.Id = ud.DocumentId
                        WHERE ud.UserId = @UserId AND ud.IsVisible = 1 AND d.IsDeleted = 0 AND d.Id = @DocumentId
	
	                    DECLARE @ResultId INT
	                    SET @ResultId = (SELECT Id FROM @Result)

                        DECLARE @ViewsNumber INT
	                    SET @ViewsNumber = (SELECT ViewsNumber FROM @Result)

                        DECLARE @MaxViewsNumber INT
	                    SET @MaxViewsNumber = (SELECT MaxViewsNumber FROM @Result)

	                    IF @ResultId IS NOT NULL
                        BEGIN
                            IF @MaxViewsNumber IS NULL OR (@MaxViewsNumber IS NOT NULL AND @ViewsNumber < @MaxViewsNumber)
                            BEGIN
		                        UPDATE UserDocuments
		                        SET ViewsNumber = ViewsNumber + 1
		                        WHERE Id = @ResultId;

                                INSERT INTO ViewLogs (DocumentId, UserId, ViewTime) VALUES (@DocumentId, @UserId, GETDATE())
                            END
                        END

                        SELECT *
	                    FROM @Result
                    END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1149), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1142) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1163), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1161) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1218), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(919), "$2a$11$InsQxfjiPEJ/jcKVmF4kjedbq0Ouic5kzc1KLOA8tlT/lxp/RxqXG" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 138, DateTimeKind.Local).AddTicks(230), "$2a$11$rWo54OuHJFWP.YRcQBDJCu6qpPj1SL3NMwH7l2k4nbFxcfJ2SsoN6" });
        }
    }
}
