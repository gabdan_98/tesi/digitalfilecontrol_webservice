﻿// <auto-generated />
using System;
using DigitalFileControl.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    [DbContext(typeof(DigitalFileControlDB))]
    [Migration("20220603161547_Added_entity_LoginLog")]
    partial class Added_entity_LoginLog
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("DigitalFileControl.Model.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("CreatorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("DeletionTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("DeletorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("DocumentTypeId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ExpirationTime")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModificationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("ModifierUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid?>("OwnerId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("PhisicalName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("DocumentTypeId");

                    b.HasIndex("OwnerId");

                    b.ToTable("Documents");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreationTime = new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(19),
                            Description = "Documento generico di delega",
                            DocumentTypeId = 1,
                            ExpirationTime = new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(10),
                            IsDeleted = false,
                            Name = "Delega",
                            OwnerId = new Guid("992be209-a765-47bd-1058-08da257a523c"),
                            PhisicalName = "delega.pdf"
                        },
                        new
                        {
                            Id = 2,
                            CreationTime = new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(49),
                            Description = "Modello per la protezione dei dati personali",
                            DocumentTypeId = 1,
                            ExpirationTime = new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(47),
                            IsDeleted = false,
                            Name = "Privacy",
                            OwnerId = new Guid("992be209-a765-47bd-1058-08da257a523c"),
                            PhisicalName = "privacy.pdf"
                        },
                        new
                        {
                            Id = 3,
                            CreationTime = new DateTime(2022, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(147),
                            Description = "Modello certificazione unica",
                            DocumentTypeId = 1,
                            ExpirationTime = new DateTime(2023, 6, 3, 18, 15, 47, 192, DateTimeKind.Local).AddTicks(132),
                            IsDeleted = false,
                            Name = "CUD",
                            OwnerId = new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                            PhisicalName = "cud.pdf"
                        });
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.DocumentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("DocumentTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "PDF"
                        });
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.ErrorLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Action")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Controller")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ErrorMessage")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ErrorTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("StackTrace")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("ErrorLogs");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.LoginLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<bool>("LoginSuccess")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LoginTime")
                        .HasColumnType("datetime2");

                    b.Property<Guid?>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("LoginLogs");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.RefreshToken", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ExpirationTime")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsExpired")
                        .HasColumnType("bit");

                    b.Property<string>("ReplacedByToken")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("RevocationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("Token")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("RefreshTokens");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("CreatorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("DeletionTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("DeletorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("ModificationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("ModifierUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = new Guid("992be209-a765-47bd-1058-08da257a523c"),
                            CreationTime = new DateTime(2022, 6, 3, 18, 15, 47, 73, DateTimeKind.Local).AddTicks(1430),
                            Email = "gabriele.dandrea001@studenti.unibas.it",
                            IsActive = true,
                            IsDeleted = false,
                            Name = "Gabriele",
                            Password = "$2a$11$o7DacwNTYMP.l3NMDdhnse7jl3DgkbI8tR7Y4xeaK4biAm.uoCDp.",
                            Surname = "D'Andrea",
                            Username = "gabriele.dandrea"
                        },
                        new
                        {
                            Id = new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                            CreationTime = new DateTime(2022, 6, 3, 18, 15, 47, 191, DateTimeKind.Local).AddTicks(9594),
                            Email = "domenico.bloisi@unibas.it",
                            IsActive = true,
                            IsDeleted = false,
                            Name = "Domenico",
                            Password = "$2a$11$TqpkPV/bVaFTNT3UkOwqOuob/4.SAuTrBCg72EIPTW5bdrqHkq5Qi",
                            Surname = "Bloisi",
                            Username = "domenico.bloisi"
                        });
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.UserDocument", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<bool>("CanDownload")
                        .HasColumnType("bit");

                    b.Property<bool>("CanPrint")
                        .HasColumnType("bit");

                    b.Property<bool>("CanView")
                        .HasColumnType("bit");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("CreatorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("DeletionTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("DeletorUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("DocumentId")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<bool>("IsVisible")
                        .HasColumnType("bit");

                    b.Property<int?>("MaxViewsNumber")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ModificationTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("ModifierUserId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("ViewsNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.HasIndex("UserId");

                    b.ToTable("UserDocuments");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CanDownload = false,
                            CanPrint = false,
                            CanView = true,
                            CreationTime = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            DocumentId = 1,
                            IsDeleted = false,
                            IsVisible = true,
                            UserId = new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                            ViewsNumber = 0
                        },
                        new
                        {
                            Id = 2,
                            CanDownload = false,
                            CanPrint = false,
                            CanView = true,
                            CreationTime = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            DocumentId = 2,
                            IsDeleted = false,
                            IsVisible = true,
                            MaxViewsNumber = 5,
                            UserId = new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                            ViewsNumber = 0
                        },
                        new
                        {
                            Id = 3,
                            CanDownload = false,
                            CanPrint = false,
                            CanView = true,
                            CreationTime = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            DocumentId = 3,
                            IsDeleted = false,
                            IsVisible = true,
                            MaxViewsNumber = 5,
                            UserId = new Guid("992be209-a765-47bd-1058-08da257a523c"),
                            ViewsNumber = 0
                        });
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.ViewLogs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("DocumentId")
                        .HasColumnType("int");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("ViewTime")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.HasIndex("UserId");

                    b.ToTable("ViewLogs");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.Document", b =>
                {
                    b.HasOne("DigitalFileControl.Model.Models.DocumentType", "DocumentType")
                        .WithMany()
                        .HasForeignKey("DocumentTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DigitalFileControl.Model.Models.User", "Owner")
                        .WithMany("OwnedDocuments")
                        .HasForeignKey("OwnerId");

                    b.Navigation("DocumentType");

                    b.Navigation("Owner");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.RefreshToken", b =>
                {
                    b.HasOne("DigitalFileControl.Model.Models.User", "User")
                        .WithMany("RefreshTokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.UserDocument", b =>
                {
                    b.HasOne("DigitalFileControl.Model.Models.Document", "Document")
                        .WithMany("UserDocuments")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DigitalFileControl.Model.Models.User", "User")
                        .WithMany("UserDocuments")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Document");

                    b.Navigation("User");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.ViewLogs", b =>
                {
                    b.HasOne("DigitalFileControl.Model.Models.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DigitalFileControl.Model.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Document");

                    b.Navigation("User");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.Document", b =>
                {
                    b.Navigation("UserDocuments");
                });

            modelBuilder.Entity("DigitalFileControl.Model.Models.User", b =>
                {
                    b.Navigation("OwnedDocuments");

                    b.Navigation("RefreshTokens");

                    b.Navigation("UserDocuments");
                });
#pragma warning restore 612, 618
        }
    }
}
