﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Updated_GetDocument_StoredProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5218), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5209) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5239), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5237) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5320), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5306) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(4881), "$2a$11$xZfnGTufoYzAY7LTng6rmOCzf41Vpo6R7cYAOtLR43qgOcypC7666" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 305, DateTimeKind.Local).AddTicks(7366), "$2a$11$ulVlQno4aYYhD47QTOMJ9eiR5WsIzM4eAi2NtIJiQyvCaN8MscEqC" });

            var sp = @"ALTER PROCEDURE [dbo].[GetDocument] 
	                    @UserId uniqueidentifier,
	                    @DocumentId int
                    AS
                    BEGIN
	                    SET NOCOUNT ON;	
	                    DECLARE @Result TABLE (Id INT, DocumentId INT, Name VARCHAR(max), Description VARCHAR(max), PhisicalName VARCHAR(max), DocumentTypeId INT, ExpirationTime DATETIME, CanView BIT, CanPrint BIT, CanDownload BIT, OwnerId UNIQUEIDENTIFIER, ViewsNumber INT, MaxViewsNumber INT)

	                    INSERT INTO @Result
	                    SELECT NULL AS Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, 1 AS CanView, 1 AS CanPrint, 1 AS CanDownload, d.OwnerId, null AS ViewsNumber, null AS MaxViewsNumber
                        FROM Documents d
                        WHERE d.IsDeleted = 0
                        AND d.Id = @DocumentId
                        AND d.OwnerId = @UserId
                        UNION
                        SELECT ud.Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, ud.CanView, ud.CanPrint, ud.CanDownload, d.OwnerId, ud.ViewsNumber, ud.MaxViewsNumber
                        FROM Documents d INNER JOIN UserDocuments ud ON d.Id = ud.DocumentId
                        WHERE ud.UserId = @UserId AND ud.IsVisible = 1 AND d.IsDeleted = 0 AND d.Id = @DocumentId
	
	                    DECLARE @ResultId INT
	                    SET @ResultId = (SELECT Id FROM @Result)

						DECLARE @CanView BIT
						SET @CanView = (SELECT CanView FROM @Result)

                        DECLARE @ViewsNumber INT
	                    SET @ViewsNumber = (SELECT ViewsNumber FROM @Result)

                        DECLARE @MaxViewsNumber INT
	                    SET @MaxViewsNumber = (SELECT MaxViewsNumber FROM @Result)

	                    IF @ResultId IS NOT NULL
                        BEGIN
                            IF ((@MaxViewsNumber IS NULL OR (@MaxViewsNumber IS NOT NULL AND @ViewsNumber < @MaxViewsNumber)) AND @CanView = 1)
                            BEGIN
		                        UPDATE UserDocuments
		                        SET ViewsNumber = ViewsNumber + 1
		                        WHERE Id = @ResultId;

                                INSERT INTO ViewLogs (DocumentId, UserId, ViewTime) VALUES (@DocumentId, @UserId, GETDATE())
                            END
                        END

                        SELECT *
	                    FROM @Result
                    END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9772), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9789), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9787) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9858), new DateTime(2023, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9843) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 712, DateTimeKind.Local).AddTicks(9462), "$2a$11$6A/gv7SF0yJvye3bv/n.RO/o/bYksg9XgXzCJyl3l8dQXIiCiPnfy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 28, 18, 43, 4, 598, DateTimeKind.Local).AddTicks(7507), "$2a$11$cQ..i9tVddiLXXJDWT2wwOXlrM5IZP//twep.10igq9Y5sdv3oTti" });
        }
    }
}
