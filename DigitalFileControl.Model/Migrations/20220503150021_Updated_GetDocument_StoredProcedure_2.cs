﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Updated_GetDocument_StoredProcedure_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1045), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1035) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1064), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1061) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1156), new DateTime(2023, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(1141) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 646, DateTimeKind.Local).AddTicks(648), "$2a$11$6Y4ah7zeCPRJhPM9tzcz0.V67oUVQBRwRdwBYUjJJIt/bbSZxMO.a" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 17, 0, 20, 532, DateTimeKind.Local).AddTicks(309), "$2a$11$/wZMm9PtNEMHW4L4HV9DOekwGXtwBdxHR09c0D7w22HJ8hti0yp9i" });

            var sp = @"ALTER PROCEDURE [dbo].[GetDocument] 
	                    @UserId uniqueidentifier,
	                    @DocumentId int
                    AS
                    BEGIN
	                    SET NOCOUNT ON;	
	                    DECLARE @Result TABLE (Id INT, DocumentId INT, Name VARCHAR(max), Description VARCHAR(max), PhisicalName VARCHAR(max), DocumentTypeId INT, ExpirationTime DATETIME, CanView BIT, CanPrint BIT, CanDownload BIT, OwnerId UNIQUEIDENTIFIER, ViewsNumber INT, MaxViewsNumber INT)

	                    INSERT INTO @Result
	                    SELECT NULL AS Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, 1 AS CanView, 1 AS CanPrint, 1 AS CanDownload, d.OwnerId, null AS ViewsNumber, null AS MaxViewsNumber
                        FROM Documents d
                        WHERE d.IsDeleted = 0
                        AND d.Id = @DocumentId
                        AND d.OwnerId = @UserId
                        UNION
                        SELECT ud.Id, d.Id AS DocumentId, d.Name, d.Description, d.PhisicalName, d.DocumentTypeId, d.ExpirationTime, ud.CanView, ud.CanPrint, ud.CanDownload, d.OwnerId, ud.ViewsNumber, ud.MaxViewsNumber
                        FROM Documents d INNER JOIN UserDocuments ud ON d.Id = ud.DocumentId
                        WHERE ud.UserId = @UserId AND ud.IsVisible = 1 AND d.IsDeleted = 0 AND ud.IsDeleted = 0 AND d.Id = @DocumentId
	
	                    DECLARE @ResultId INT
	                    SET @ResultId = (SELECT Id FROM @Result)

						DECLARE @CanView BIT
						SET @CanView = (SELECT CanView FROM @Result)

                        DECLARE @ViewsNumber INT
	                    SET @ViewsNumber = (SELECT ViewsNumber FROM @Result)

                        DECLARE @MaxViewsNumber INT
	                    SET @MaxViewsNumber = (SELECT MaxViewsNumber FROM @Result)

	                    IF @ResultId IS NOT NULL
                        BEGIN
                            IF ((@MaxViewsNumber IS NULL OR (@MaxViewsNumber IS NOT NULL AND @ViewsNumber < @MaxViewsNumber)) AND @CanView = 1)
                            BEGIN
		                        UPDATE UserDocuments
		                        SET ViewsNumber = ViewsNumber + 1
		                        WHERE Id = @ResultId;

                                INSERT INTO ViewLogs (DocumentId, UserId, ViewTime) VALUES (@DocumentId, @UserId, GETDATE())
                            END
                        END

                        SELECT *
	                    FROM @Result
                    END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6309), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6300) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6334), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6332) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6409), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6391) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(5938), "$2a$11$LF.mGYZ.Ueb2lD4kFmG4eueqDgN0/yO4.u5oaDtf9bJHb769iPvs2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 690, DateTimeKind.Local).AddTicks(862), "$2a$11$9tf.FGK/vnW6sDsO0sJiE.Fj1RyeX043nOIDUVEE4XSVJCivZzwea" });
        }
    }
}
