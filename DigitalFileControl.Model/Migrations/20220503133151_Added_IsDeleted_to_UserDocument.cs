﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Added_IsDeleted_to_UserDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "UserDocuments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6309), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6300) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6334), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6332) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6409), new DateTime(2023, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(6391) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 804, DateTimeKind.Local).AddTicks(5938), "$2a$11$LF.mGYZ.Ueb2lD4kFmG4eueqDgN0/yO4.u5oaDtf9bJHb769iPvs2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 5, 3, 15, 31, 50, 690, DateTimeKind.Local).AddTicks(862), "$2a$11$9tf.FGK/vnW6sDsO0sJiE.Fj1RyeX043nOIDUVEE4XSVJCivZzwea" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "UserDocuments");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5218), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5209) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5239), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5237) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5320), new DateTime(2023, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(5306) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 419, DateTimeKind.Local).AddTicks(4881), "$2a$11$xZfnGTufoYzAY7LTng6rmOCzf41Vpo6R7cYAOtLR43qgOcypC7666" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 30, 11, 45, 51, 305, DateTimeKind.Local).AddTicks(7366), "$2a$11$ulVlQno4aYYhD47QTOMJ9eiR5WsIzM4eAi2NtIJiQyvCaN8MscEqC" });
        }
    }
}
