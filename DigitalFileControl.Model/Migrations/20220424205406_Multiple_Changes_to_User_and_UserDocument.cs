﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Multiple_Changes_to_User_and_UserDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Users",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletorUserId",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "UserDocuments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatorUserId",
                table: "UserDocuments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "UserDocuments",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletorUserId",
                table: "UserDocuments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModificationTime",
                table: "UserDocuments",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifierUserId",
                table: "UserDocuments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Documents",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletorUserId",
                table: "Documents",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1149), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1142) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1163), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1161) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1218), new DateTime(2023, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Email", "Name", "Password", "Surname" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 252, DateTimeKind.Local).AddTicks(919), "domenico.bloisi@unibas.it", "Domenico", "$2a$11$InsQxfjiPEJ/jcKVmF4kjedbq0Ouic5kzc1KLOA8tlT/lxp/RxqXG", "Bloisi" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Email", "Name", "Password", "Surname" },
                values: new object[] { new DateTime(2022, 4, 24, 22, 54, 6, 138, DateTimeKind.Local).AddTicks(230), "gabriele.dandrea001@studenti.unibas.it", "Gabriele", "$2a$11$rWo54OuHJFWP.YRcQBDJCu6qpPj1SL3NMwH7l2k4nbFxcfJ2SsoN6", "D'Andrea" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DeletorUserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "DeletorUserId",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "ModificationTime",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "ModifierUserId",
                table: "UserDocuments");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "DeletorUserId",
                table: "Documents");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8439), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8431) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8463), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8461) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreationTime", "ExpirationTime" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8541), new DateTime(2023, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8529) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 600, DateTimeKind.Local).AddTicks(8100), "$2a$11$AqxymqwtqYhi4MNHLQnrQOjjoyXQRbzmTalEKYikjzkHtWKXVvSHu" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"),
                columns: new[] { "CreationTime", "Password" },
                values: new object[] { new DateTime(2022, 4, 24, 16, 37, 36, 487, DateTimeKind.Local).AddTicks(6353), "$2a$11$Sagq44fGoH7OG5Pu./zyDeU1Z.jEr7YeF0kDyZ7sIZznebvfdKh8." });
        }
    }
}
