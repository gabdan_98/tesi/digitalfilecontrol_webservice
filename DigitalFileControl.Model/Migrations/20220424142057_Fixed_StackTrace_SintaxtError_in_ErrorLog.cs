﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Fixed_StackTrace_SintaxtError_in_ErrorLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StrackTrace",
                table: "ErrorLogs",
                newName: "StackTrace");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "IsActive", "ModificationTime", "ModifierUserId", "Password", "Username" },
                values: new object[] { new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"), new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5099), null, true, null, null, "$2a$11$ZbjHk4sGnBMBrs54d0l/3.oG6NU9ogybHa8Y23lGlT3TUYeHjn7bi", "domenico.bloisi" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "IsActive", "ModificationTime", "ModifierUserId", "Password", "Username" },
                values: new object[] { new Guid("992be209-a765-47bd-1058-08da257a523c"), new DateTime(2022, 4, 24, 16, 20, 57, 279, DateTimeKind.Local).AddTicks(5263), null, true, null, null, "$2a$11$IjBTbhlRRTYrU5H1zWWhIex.c.XbA72pyl/TNv7NvgASF7nDwtC/q", "gabriele.dandrea" });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "Description", "DocumentTypeId", "ExpirationTime", "IsDeleted", "ModificationTime", "ModifierUserId", "Name", "OwnerId", "PhisicalName" },
                values: new object[] { 1, new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5339), null, "Documento generico di delega", 1, new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5330), false, null, null, "Delega", new Guid("992be209-a765-47bd-1058-08da257a523c"), "delega.pdf" });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "Description", "DocumentTypeId", "ExpirationTime", "IsDeleted", "ModificationTime", "ModifierUserId", "Name", "OwnerId", "PhisicalName" },
                values: new object[] { 2, new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5357), null, "Modello per la protezione dei dati personali", 1, new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5355), false, null, null, "Privacy", new Guid("992be209-a765-47bd-1058-08da257a523c"), "privacy.pdf" });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "CreationTime", "CreatorUserId", "Description", "DocumentTypeId", "ExpirationTime", "IsDeleted", "ModificationTime", "ModifierUserId", "Name", "OwnerId", "PhisicalName" },
                values: new object[] { 3, new DateTime(2022, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5407), null, "Modello certificazione unica", 1, new DateTime(2023, 4, 24, 16, 20, 57, 393, DateTimeKind.Local).AddTicks(5398), false, null, null, "CUD", new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"), "cud.pdf" });

            migrationBuilder.InsertData(
                table: "UserDocuments",
                columns: new[] { "Id", "CanDownload", "CanPrint", "CanView", "DocumentId", "IsVisible", "MaxViewsNumber", "UserId", "ViewsNumber" },
                values: new object[] { 1, false, false, true, 1, true, null, new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"), 0 });

            migrationBuilder.InsertData(
                table: "UserDocuments",
                columns: new[] { "Id", "CanDownload", "CanPrint", "CanView", "DocumentId", "IsVisible", "MaxViewsNumber", "UserId", "ViewsNumber" },
                values: new object[] { 2, false, false, true, 2, true, 5, new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"), 0 });

            migrationBuilder.InsertData(
                table: "UserDocuments",
                columns: new[] { "Id", "CanDownload", "CanPrint", "CanView", "DocumentId", "IsVisible", "MaxViewsNumber", "UserId", "ViewsNumber" },
                values: new object[] { 3, false, false, true, 3, true, 5, new Guid("992be209-a765-47bd-1058-08da257a523c"), 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserDocuments",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserDocuments",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "UserDocuments",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8ac1fdbf-d60b-4519-8c20-08da257f073f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("992be209-a765-47bd-1058-08da257a523c"));

            migrationBuilder.RenameColumn(
                name: "StackTrace",
                table: "ErrorLogs",
                newName: "StrackTrace");
        }
    }
}
