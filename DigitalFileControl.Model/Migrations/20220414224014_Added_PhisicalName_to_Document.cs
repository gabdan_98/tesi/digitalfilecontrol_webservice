﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DigitalFileControl.Model.Migrations
{
    public partial class Added_PhisicalName_to_Document : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhisicalName",
                table: "Documents",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhisicalName",
                table: "Documents");
        }
    }
}
